import { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Header from './Components/Header';
import ProductPage from './Components/ProductPage';
import CartPage from './Components/Cart';

const App = () => {
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState([]);
  const [sliderValue, setSliderValue] = useState(100);
  const [searchValue, setSearchValue] = useState('');
  const [category, setCategory] = useState('all');
  const [brand, setBrand] = useState('all');
  const [filter, setFilter] = useState(products);
  const [totals, setTotals] = useState({
    subtotal: 0,
    shipping: 0,
    total: 0,
  });

  useEffect(() => {
    const getProducts = async () => {
      const productsFromServer = await fetchProducts();
      setProducts(productsFromServer);
      setFilter(
        productsFromServer.filter(
          (product) => product.price < sliderValue,
        ),
      );
    };
    const getCart = async () => {
      const cartFromServer = await fetchCart();
      setCart(cartFromServer);
    };
    getProducts();
    getCart();
  }, []);

  useEffect(() => {
    updateFiltering();
    updateTotals();
  }, [category, brand, sliderValue, searchValue, cart]);

  // Fetch products
  const fetchProducts = async () => {
    const res = await fetch('http://localhost:3000/products');
    const data = await res.json();
    // Firing multiple promises
    // Results at different times
    // Make purchase manipulate promise
    return data;
  };

  const fetchProduct = async (id) => {
    const res = await fetch(`http://localhost:3000/products/${id}`);
    const data = await res.json();

    return data;
  };

  const fetchCartItem = async (id) => {
    const res = await fetch(`http://localhost:3000/cart/${id}`);
    const data = await res.json();

    return data;
  };

  // Fetch cart
  const fetchCart = async () => {
    const res = await fetch('http://localhost:3000/cart');
    const data = await res.json();

    return data;
  };

  const changeQuantity = async (e, cartItem) => {
    console.log(e.target);
    const cartItemToChange = await fetchCartItem(cartItem.id);
    let updatedCartItem;
    if (e.target.name === 'more') {
      updatedCartItem = {
        ...cartItemToChange,
        quantity: (cartItemToChange.quantity += 1),
      };
    } else {
      updatedCartItem = {
        ...cartItemToChange,
        quantity: (cartItemToChange.quantity -= 1),
      };
    }

    const res = await fetch(
      `http://localhost:3000/cart/${cartItem.id}`,
      {
        method: 'PUT',
        headers: {
          'Content-type': 'application/json',
        },
        body: JSON.stringify(updatedCartItem),
      },
    );

    const data = await res.json();

    setCart(
      cart.map((cartItem) =>
        cartItem.id === data.id
          ? { ...cartItem, quantity: data.quantity }
          : cartItem,
      ),
    );
    //setSubtotal((subtotal = data.price * data.quantity));
    updateTotals();
  };

  const addProduct = async (product) => {
    // check if product is already in cart
    if (
      cart.filter(
        (productToCheck) => productToCheck.id === product.id,
      ).length === 0
    ) {
      //Get the product we want to update
      const productToToggle = await fetchProduct(product.id);
      const updatedProduct = { ...productToToggle, isInCart: true };
      const resProduct = await fetch(
        `http://localhost:3000/products/${product.id}`,
        {
          method: 'PUT',
          headers: {
            'Content-type': 'application/json',
          },
          body: JSON.stringify(updatedProduct),
        },
      );

      const dataProduct = await resProduct.json();

      setProducts(
        products.map((product) =>
          product.id === dataProduct.id
            ? { ...product, isInCart: true }
            : product,
        ),
      );

      // Now we can push the item to the cart
      const res = await fetch('http://localhost:3000/cart', {
        method: 'POST',
        headers: {
          'Content-type': 'application/json',
        },
        body: JSON.stringify({ ...product, quantity: 1 }),
      });

      const data = await res.json();
      //set the cart with the new item
      setCart([...cart, data]);
      updateTotals();
    } else {
      //Product is already in the users cart
      // Remove the item from our cart
      //Get the product we want to update
      const productToToggle = await fetchProduct(product.id);
      const updatedProduct = { ...productToToggle, isInCart: false };

      const resProduct = await fetch(
        `http://localhost:3000/products/${product.id}`,
        {
          method: 'PUT',
          headers: {
            'Content-type': 'application/json',
          },
          body: JSON.stringify(updatedProduct),
        },
      );

      const dataProduct = await resProduct.json();

      setProducts(
        products.map((product) =>
          product.id === dataProduct.id
            ? { ...product, isInCart: false }
            : product,
        ),
      );

      // Now we can delete the item from the users cart
      await fetch(`http://localhost:3000/cart/${product.id}`, {
        method: 'DELETE',
      });

      //Get the updated cart
      const cartFromServer = await fetchCart();

      // Set the state cart variable
      setCart(cartFromServer);
    }
  };

  const updateTotals = () => {
    const cartSubtotal = cart.reduce(
      (a, b) => a + (b.price * b.quantity || 0),
      0,
    );
    const cartItemCount = cart.reduce(
      (a, b) => a + (b.quantity || 0),
      0,
    );
    let shippingCost = 0;
    if (cartItemCount > 3 && cartItemCount < 6) {
      shippingCost = 15;
    } else if (cartItemCount >= 6 && cartItemCount < 15) {
      shippingCost = 60;
    } else if (cartItemCount >= 15) {
      shippingCost = 100;
    }
    const total = cartSubtotal * 1.13;
    setTotals({
      ...totals,
      subtotal: cartSubtotal,
      shipping: shippingCost,
      total: total,
    });
    console.log(totals);
  };

  const updateFiltering = () => {
    // Now filter the front end
    let result = products;
    if (category !== 'all') {
      result = result.filter(
        (product) => product.category === category,
      );
    }
    if (brand !== 'all') {
      result = result.filter((product) => product.brand === brand);
    }
    if (searchValue !== '') {
      result = result.filter((product) =>
        product.name
          .toLowerCase()
          .includes(searchValue.toLowerCase()),
      );
    }

    result = result.filter((product) => product.price < sliderValue);

    setFilter(result);
  };

  const onFilterChange = (e) => {
    const name = e.target.getAttribute('name');

    if (name === 'categoryChanger') {
      setCategory(e.target.value);
    } else if (name === 'brandChanger') {
      setBrand(e.target.value);
    } else if (name === 'maxPriceChanger') {
      setSliderValue(e.target.value);
    } else if (name === 'searchChanger') {
      setSearchValue(e.target.value);
    } else {
      console.log('Do nothing');
    }
  };

  return (
    <Router>
      <div className="container">
        <Header title="Gamebuy.com" />
        <Route
          path="/"
          exact
          render={(props) => (
            <ProductPage
              title="Video Game Products on Sale"
              products={filter}
              onAddProduct={addProduct}
              sliderValue={sliderValue}
              onFilterChange={onFilterChange}
            />
          )}
        />
        <Route
          path="/cart"
          render={(props) => (
            <CartPage
              cart={cart}
              onChangeQuantity={changeQuantity}
              title="Your Cart"
              totals={totals}
              onAddProduct={addProduct}
            />
          )}
        />
      </div>
    </Router>
  );
};

export default App;
