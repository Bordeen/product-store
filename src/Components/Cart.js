import React from 'react';
import styled from 'styled-components';
import { FaMinus, FaPlus, FaTrashAlt } from 'react-icons/fa';

const PageTitle = styled.p`
  margin-top: 0;
  padding-top: 1rem;
  font-size: 2rem;
  font-weight: regular;
`;

const PageWrapper = styled.div`
  margin: 0 auto;
  width: 1200px;
`;

const CartItemCard = styled.div`
  padding: 1rem;
  display: grid;
  grid-template-columns: auto 8fr;
  grid-template-rows: auto auto auto;
`;

const CartItemImage = styled.img`
  width: 150px;
`;

const CartItemName = styled.p``;

const CartItemPrice = styled.p`
  font-weight: bold;
  margin-left: auto;
`;
const CartItemQuantity = styled.p`
  font-weight: bold;
  justify-self: center;
  margin-top: 7px;
  padding-left: 1rem;
  padding-right: 1rem;
`;

const CartItemQuantityBtn = styled.button`
  justify-self: center;
  font-weight: bold;
  border-radius: 50%;
  width: 2rem;
  height: 2rem;
  background-color: #011627;
  color: white;
  border-color: none;
`;

const CartItemQuantityWrap = styled.div`
  display: grid;
  grid-template-columns: auto auto auto;
  grid-template-rows: auto;
  width: fit-content;
`;

const CartItemDetails = styled.div`
  padding-left: 1rem;
  display: grid;
  grid-template-columns: auto auto;
  grid-template-rows: auto auto auto;
`;

const CartWrapper = styled.div`
  display: grid;
  grid-template-columns: 7fr 3fr;
`;

const ShippingTitle = styled.p`
  font-size: 1.5rem;
  margin-bottom: 0;
  margin-top: 0;
`;

const ShippingTitleDesc = styled.p`
  font-size: 0.8rem;
  color: gray;
`;

const ShippingPostalInput = styled.input`
  width: 100%;
  height: 25px;
`;

const ShippingPostalUpdateBtn = styled.button``;

const ShippingPostalWrapper = styled.div`
  display: grid;
  grid-template-columns: auto auto;
  grid-template-rows: auto;
`;

const ProductSubtotalWrap = styled.div`
  display: grid;
  grid-template-columns: 9fr 1fr;
  grid-template-rows: auto;
`;

const GrandTotal = styled.p`
  font-weight: bold;
`;

const CheckoutBtn = styled.button`
  width: 100%;
  color: black;
  font-weight: bold;
  background-color: #2ec4b6;
  border: none;
  border-radius: 0.2rem;
  height: 2rem;
`;

const RemoveText = styled.a`
  font-weight: bold;
  &:hover {
    text-decoration: underline;
    cursor: pointer;
  }
`;

export const Cart = ({
  cart,
  title,
  onChangeQuantity,
  totals,
  onAddProduct,
}) => {
  const cartItems = cart.map((item) => (
    <CartItemCard key={item.id}>
      <CartItemImage src={item.imageURL} />
      <CartItemDetails>
        <CartItemName>{item.name}</CartItemName>
        <CartItemPrice>${item.price}</CartItemPrice>
        <CartItemQuantityWrap>
          <CartItemQuantityBtn
            name="less"
            onClick={(e) => onChangeQuantity(e, item)}
            disabled={item.quantity <= 1}
          >
            <FaMinus
              style={{
                pointerEvents: 'none',
              }}
            />
          </CartItemQuantityBtn>
          <CartItemQuantity>{item.quantity}</CartItemQuantity>
          <CartItemQuantityBtn
            name="more"
            onClick={(e) => onChangeQuantity(e, item)}
          >
            <FaPlus
              style={{
                pointerEvents: 'none',
              }}
            />
          </CartItemQuantityBtn>
        </CartItemQuantityWrap>
        <span></span>
        <div>
          <FaTrashAlt
            style={{
              display: 'inline',
            }}
          />
          <RemoveText
            onClick={() => onAddProduct(item)}
            style={{
              display: 'inline',
              paddingLeft: '0.5rem',
            }}
          >
            Remove
          </RemoveText>
        </div>
      </CartItemDetails>
    </CartItemCard>
  ));

  return (
    <PageWrapper>
      <PageTitle>{title}</PageTitle>
      <CartWrapper>
        <div
          style={{
            borderRadius: '1rem',
            backgroundColor: '#ced4da',
            padding: '1rem',
          }}
        >
          {cartItems.length > 0 ? (
            <>{cartItems}</>
          ) : (
            <>
              <h2>This cart is empty!</h2>
              <p>Why not fill it up?</p>
            </>
          )}
        </div>
        <div
          style={{
            padding: '1rem',
          }}
        >
          <ShippingTitle>Enter your postal code</ShippingTitle>
          <ShippingTitleDesc>
            We will use it to get you a shipping cost estimate.
          </ShippingTitleDesc>
          <ShippingPostalWrapper>
            <ShippingPostalInput type="text"></ShippingPostalInput>
            <ShippingPostalUpdateBtn>UPDATE</ShippingPostalUpdateBtn>
          </ShippingPostalWrapper>
          <h2>Order Summary</h2>
          <ProductSubtotalWrap>
            <p>Product Subtotal:</p>
            <p>${totals.subtotal.toFixed(2)}</p>
          </ProductSubtotalWrap>
          <ProductSubtotalWrap>
            <p>Estimated Shipping Cost:</p>
            <p>
              {totals.shipping === 0
                ? 'FREE'
                : `$${totals.shipping.toFixed(2)}`}
            </p>
          </ProductSubtotalWrap>
          <ProductSubtotalWrap>
            <GrandTotal>Estimated Total:</GrandTotal>
            <GrandTotal>${totals.total.toFixed(2)}</GrandTotal>
          </ProductSubtotalWrap>
          <CheckoutBtn>Continue to Checkout</CheckoutBtn>
        </div>
      </CartWrapper>
    </PageWrapper>
  );
};

export default Cart;
