import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import bannerImage from '../Assets/header_banner.jpg';
import { Link } from 'react-router-dom';
import { FaShoppingCart } from 'react-icons/fa';

const NavHeader = styled.div`
  background-image: url(${bannerImage});
  background-size: cover;
  padding-bottom: 0;
`;

const NavContent = styled.div`
  margin: 0 auto;
  width: 1200px;
`;

const NavTitle = styled.h1`
  color: white;
  padding: 1rem;
  width: fit-content;
  background: rgba(0, 0, 0, 0.8);
  margin: auto;
`;

const NavItem = styled.button`
  background-color: #2ec4b6;
  padding: 5px;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  float: ${(props) => (props.floatRight ? 'right' : 'unset')};
  color: black;
  font-weight: bold;
`;

const navHeadings = ['Game Products', 'Cart'];

const navItems = [];

for (const [index, value] of navHeadings.entries()) {
  if (index === navHeadings.length - 1) {
    navItems.push(
      <Link to="/cart">
        <NavItem key={index} active floatRight>
          {value} <FaShoppingCart />
        </NavItem>
      </Link>,
    );
  } else {
    navItems.push(
      <Link to="/">
        <NavItem key={index} active>
          {value}
        </NavItem>
      </Link>,
    );
  }
}

const Header = ({ title }) => {
  return (
    <NavHeader>
      <NavContent>
        <NavTitle active>{title}</NavTitle>
        {navItems}
      </NavContent>
    </NavHeader>
  );
};

Header.propTypes = {
  title: PropTypes.string,
};

export default Header;
