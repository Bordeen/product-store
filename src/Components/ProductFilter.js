import React from 'react';
import styled from 'styled-components';

const SliderWrapper = styled.div``;

const Slider = styled.input`
  background: linear-gradient(to right, #10d5c2 0%, #eaeefb 100%);
  border-radius: 8px;
  height: 8px;
  width: 100%;
  outline: none;
  transition: all 450ms ease-in;
  -webkit-appearance: none;
`;

const DropdownCategory = styled.select`
  width: 100%;
  margin-bottom: 1rem;
`;

const DropdownBrand = styled.select`
  width: 100%;
  margin-bottom: 1rem;
`;

const SearchBar = styled.input`
  width: 100%;
`;

export const ProductFilter = ({ sliderValue, onFilterChange }) => {
  return (
    <div>
      <SliderWrapper>
        <SearchBar
          name="searchChanger"
          onKeyUp={onFilterChange}
          type="text"
          placeholder="Search"
        />
        <p>Category</p>
        <DropdownCategory
          name="categoryChanger"
          onChange={onFilterChange}
        >
          <option value="all">All</option>
          <option value="game">Games</option>
          <option value="console">Consoles</option>
          <option value="accessory">Accessories</option>
          <option value="clothing">Clothing</option>
        </DropdownCategory>
        <p>Brand</p>
        <DropdownBrand name="brandChanger" onChange={onFilterChange}>
          <option value="all">All</option>
          <option value="nintendo">Nintendo</option>
          <option value="xbox">Xbox</option>
          <option value="playstation">Playstation</option>
          <option value="other">Other...</option>
        </DropdownBrand>
        <p>Max Price: ${sliderValue}</p>
        <Slider
          name="maxPriceChanger"
          type="range"
          min={0}
          max={500}
          step={25}
          value={sliderValue}
          onChange={onFilterChange}
        />
      </SliderWrapper>
    </div>
  );
};

export default ProductFilter;
