import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Products from './Products';
import ProductFilter from './ProductFilter';

const Wrapper = styled.div`
  background-color: white;
  height: 100vh;
  margin: 0 auto;
  width: 1200px;
  height: 100%;

  @media (max-width: 1024px) {
    grid-template-columns: auto;
  }

  @media (max-width: 768px) {
    grid-template-columns: auto;
  }
`;

const PageTitle = styled.h1`
  margin-top: 0;
  padding-top: 1rem;
`;

const ProductWrapperGrid = styled.div`
  display: grid;
  grid-template-columns: 2fr 8fr;
  grid-template-rows: auto auto auto;
`;

const PageContent = ({
  title,
  products,
  cart,
  onAddProduct,
  sliderValue,
  onFilterChange,
}) => {
  return (
    <>
      <Wrapper>
        <PageTitle>{title}</PageTitle>
        <ProductWrapperGrid>
          <ProductFilter
            sliderValue={sliderValue}
            onFilterChange={onFilterChange}
          ></ProductFilter>
          <Products
            products={products}
            onAddProduct={onAddProduct}
          ></Products>
        </ProductWrapperGrid>
      </Wrapper>
    </>
  );
};

PageContent.propTypes = {
  title: PropTypes.string,
};

export default PageContent;
