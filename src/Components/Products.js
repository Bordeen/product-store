import React from 'react';
import styled from 'styled-components';

const ProductCardWrapper = styled.div`
  width: 80%;
  display: grid;
  grid-template-columns: auto auto auto auto;
  grid-gap: 1rem;
  margin: auto;
  margin-bottom: 1rem;
  @media (max-width: 1224px) {
    grid-template-columns: auto auto auto;
  }
  @media (max-width: 768px) {
    grid-template-columns: auto auto;
    padding: 1rem;
    margin-top: 2rem;
  }
  @media (max-width: 481px) {
    grid-template-columns: auto;
    padding: 1rem;
    margin-top: 2rem;
  }
`;

const ProductCard = styled.div`
  background-color: #ced4da;
  width: 200px;
  height: auto;
  justify-self: center;
  display: flex;
  flex-direction: column;
  border: solid 1px gray;
  border-radius: 0.5rem;

  @media (max-width: 481px) {
    width: 250px;
  }
`;

const ProductImage = styled.img`
  width: 80%;
  height: 170px;
  margin: auto;
  padding: 1rem;
`;

const ProductName = styled.p`
  color: black;
  padding-left: 1rem;
  margin-bottom: 0;
  margin-top: 0.5rem;
`;

const ProductPrice = styled.h3`
  color: black;
  padding-left: 1rem;
`;

const ProductDesc = styled.div`
  background-color: white;
  padding: 1rem;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  border-radius: 0.5rem;
  box-shadow: 0px 0 10px rgba(0, 0, 0, 0.8);
`;

const PurchaseBtnWrapper = styled.div`
  width: 100%;
  text-align: center;
`;

const PurchaseBtn = styled.button`
  background-color: #2ec4b6;
  border: none;
  width: 75%;
  color: white;
  border-radius: 1rem;
  justify-content: center;
  font-weight: bold;
  margin-top: 1rem;
`;

const Products = ({ products, onAddProduct }) => {
  const productList = products.map((product) => (
    <ProductCard key={product.id}>
      <ProductImage src={product.imageURL}></ProductImage>
      <ProductName>{product.name}</ProductName>
      <ProductPrice>${product.price}</ProductPrice>
      <ProductDesc>
        {product.description}
        <PurchaseBtnWrapper>
          <PurchaseBtn onClick={() => onAddProduct(product)}>
            {product.isInCart ? 'Remove from cart' : 'Add to cart'}
          </PurchaseBtn>
        </PurchaseBtnWrapper>
      </ProductDesc>
    </ProductCard>
  ));
  return (
    <ProductCardWrapper>
      {productList.length > 0 ? (
        productList
      ) : (
        <h1>No items :( Please adjust your filters</h1>
      )}
    </ProductCardWrapper>
  );
};

export default Products;
